﻿using System;
using UnityEngine;

// How to use:
//  - Call a multicast message by calling "SomeActionMulticast"
//    This will execute the message in all components that have subscribed
//  - Call an unicast message with a GameObject parameter.
//    This will execute the message in all components of the GameObject that have subscribed
public class Messages
{
	// Multicast messages (multiple objects)
	// Sent to all components that subscribe for them, in any GameObject
	// These are purely C# events
	public delegate void SomeActionMulticastDelegate(int value);
	public static event SomeActionMulticastDelegate SomeActionMulticastEvent;
	public static void SomeActionMulticast(int value)
	{
		SomeActionMulticastEvent?.Invoke(value);
	}

	// Unicast message (single object)
	// Sent to all components that subscribe for them, in a single GameObject
	public static void SomeActionUnicast(GameObject gameObject, int value)
	{
		MessageVoidImpl(gameObject, component => component.SomeActionUnicast(value));
	}

	// Unicase message with result
	public static int GetIntValue(GameObject gameObject)
	{
		return MessageValueImpl<int>(gameObject, component => component.GetIntValue(), 0);
	}

	// Implementation helpers
	private static void MessageVoidImpl(GameObject gameObject, Action<MessageReceiver> method)
	{
		foreach(var component in gameObject.GetComponents<MessageReceiver>())
		{
			method(component);
		}
	}

	private static T MessageValueImpl<T>(GameObject gameObject, Func<MessageReceiver, T> method, T defaultValue)
	{
		foreach(var component in gameObject.GetComponents<MessageReceiver>())
		{
			var result = method(component);
			if(!result.Equals(defaultValue))
			{
				return result;
			}
		}

		return defaultValue;
	}
}
