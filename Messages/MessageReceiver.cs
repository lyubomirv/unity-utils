﻿using UnityEngine;

// All components that want to wrok with message must extend this class,
// instead of MonoBehaviour
public class MessageReceiver : MonoBehaviour
{
	protected delegate void SomeActionUnicastDelegate(int value);
	protected event SomeActionUnicastDelegate SomeActionUnicastEvent;
	public void SomeActionUnicast(int value)
	{
		SomeActionUnicastEvent?.Invoke(value);
	}

	protected delegate int GetIntValueDelegate();
	protected event GetIntValueDelegate GetIntValueEvent;
	public int GetIntValue()
	{
		var handler = GetIntValueEvent;
		if(handler == null) return 0;

		return handler();
	}
}
